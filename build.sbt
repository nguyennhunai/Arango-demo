name := "extract_feature"

version := "0.1"

scalaVersion := "2.11.0"
resolvers += "SparkPackages" at "https://dl.bintray.com/spark-packages/maven"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.3",
  "org.apache.spark" %% "spark-sql" % "2.4.3",
  "com.arangodb" % "arangodb-spark-connector" % "1.0.2",
  "org.apache.spark" %% "spark-graphx" % "2.4.3",
  "graphframes" % "graphframes" % "0.7.0-spark2.4-s_2.11"

)

import com.arangodb.spark.{ArangoSpark, ReadOptions, WriteOptions}
import org.apache.spark._

import scala.beans.BeanProperty
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types.DoubleType
import org.graphframes.GraphFrame

class ClusteringCoefficient(g: GraphFrame) {
  def triangleCount(): DataFrame = {
    g.triangleCount.run()
  }

  def calculate_local_CC(triangle_count: Int, vertice_degree: Int): Double = {
    if (vertice_degree <= 1 || triangle_count == 0) 0.0
    else triangle_count * 2.0 / (vertice_degree * (vertice_degree - 1))
  }

  def local_clustering_coefficient(): DataFrame = {
    val calculate_local_CC_udf = udf((triangle_count: Long, vertice_degree: Int) => if (vertice_degree <= 1 || triangle_count == 0) 0.0
    else triangle_count * 2.0 / (vertice_degree * (vertice_degree - 1))
      , DoubleType)
    val degrees = g.degrees
    val tringleCountingDF = g.triangleCount.run()
    val result = tringleCountingDF
      .join(degrees, Seq("id"), joinType = "leftouter")
      .na.fill(0)
      .withColumn("CC", calculate_local_CC_udf(col("count"), col("degree")))
      .drop("count")
      .drop("degree")
    result
  }

}

object extract_feature {

  case class Airports(@BeanProperty name: String,
                      @BeanProperty city: String,
                      @BeanProperty state: String,
                      @BeanProperty country: String,
                      @BeanProperty vip: Boolean,
                      _id: String) {
    def this() = this(name = null, city = null, state = null, country = null, vip = false, _id = null)
  }

  case class Flights(@BeanProperty Year: Int,
                     @BeanProperty Month: Int,
                     @BeanProperty Day: Int,
                     @BeanProperty DayOfWeek: Int,
                     @BeanProperty DepTime: Int,
                     @BeanProperty ArrTime: Int,
                     @BeanProperty DepTimeUTC: String,
                     @BeanProperty ArrTimeUTC: String,
                     @BeanProperty UniqueCarrier: String,
                     @BeanProperty FlightNum: Int,
                     @BeanProperty TailNum: String,
                     @BeanProperty Distance: Int,
                     _from: String,
                     _to: String) {
    def this() = this(Year = 0, Month = 0, Day = 0, DayOfWeek = 0,
      DepTime = 0, ArrTime = 0, DepTimeUTC = null, ArrTimeUTC = null, UniqueCarrier = null, FlightNum = 0, TailNum = null, Distance = 0, _from = null, _to = null)
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .set("arangodb.hosts", "10.159.19.100:8529,10.159.19.101:8529,10.159.19.102:8529")
      .set("arangodb.port", "8529")
      .set("arangodb.user", "root")
      .set("arangodb.password", "root")
      .setAppName("SparkToArango2")
      .setMaster("local[*]")
    val spark = SparkSession.
      builder
      .config(conf)
      .getOrCreate()
    import spark.implicits._
    val sc = spark.sparkContext
    spark.sparkContext.setLogLevel("ERROR")
    val edgesRDD = ArangoSpark.load[Flights](sc, "edges_30M")
    val edges = edgesRDD.toDS().select("_from", "_to").toDF("src", "dst").distinct()
    val verticlesRDD = ArangoSpark.load[Airports](sc, "vertices_30M")
    val verticles = verticlesRDD.toDS().select("_id").toDF("id").distinct()
    val g = GraphFrame(verticles, edges)
    val algo = new ClusteringCoefficient(g)
    val result = algo.triangleCount()
    result.cache()
    result.count()
    val t1 = System.nanoTime
    ArangoSpark.save(result,"graph30M_result")
    val duration = (System.nanoTime - t1) / 1e9d
    println(duration)
    println("Finish")
  }
}
